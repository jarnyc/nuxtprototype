# s4m

## Build Setup

```bash
# install dependencies
$ npm install bootstrap-vue
$ npm install sass-loader 
$ npm install node-sass
$ npm add @nuxtjs/axios 

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

Open source svg icons:
https://remixicon.com/

